#!/usr/bin/python
# -*- coding: utf8 -*-

import os
import time
import json
import requests
import logging
import traceback
import hashlib
from multiprocessing import Process

if __name__ == '__main__':
    import sys
    sys.path.append('..')

import data_access.settings
from utility import setLogging
from utility import version, ramfs
from utility import robotId

__all__ = [
        'serverAPI',
        ]

LOGIN_URL_POSTFIX       = '/medical/auth/robot/login'       # 机器人登录地址后缀
GET_TOKEN_URL_POSTFIX   = '/medical/auth/api/getLoginToken' # 机器人获取登录令牌后缀
CONFIG_URL_POSTFIX      = '/medical/robot/findMyConfig'     # 机器人配置信息地址后缀
HEATBEAT_URL_POSTFIX    = '/medical/robot/heartbeat'        # 机器人心跳地址后缀
MP3_LIST_URL_POSTFIX    = '/medical/robot/listMp3'          # 音频列表地址后缀
DOCTOR_LIST_URL_POSTFIX = '/medical/robot/listOnlineDoctorByCallOrder'  # 在线医生（已排序）列表地址
CALL_LOG_URL_POSTFIX    = '/medical/robot/saveCallLog'      # 保存呼叫记录

VERSION_URL_POSTFIX     = '/medical/basic/config/robot/version' # 获取机器人固件地址后缀

# 服务器接口类
class serverAPI(object):
    # 初始化
    def __init__(self, hostName, portNumber, cbGetRobotId = robotId.robotIdSim):
        self._hostName = hostName
        self._portNumber = portNumber
        self._cbGetRobotId = cbGetRobotId
        self._token = None
        self._rbtId = None
        self._code  = 404
        self._https = True if 'https' in hostName else False
        self._personList = []
        self._updateDir = ramfs.ramfsInit()

        _, settings = data_access.settings.get_settings()
        self._gps = settings['gpsCoord']
        requests.packages.urllib3.disable_warnings()

    # 获取登录令牌
    def getLoginToken(self):
        logging.debug('serverAPI.getLoginToken() start.')
        ret, loginToken = False, None
        try:
            tokenUrl = self._hostName + ':' + self._portNumber + GET_TOKEN_URL_POSTFIX
            logging.debug('get login token: url - %s' %tokenUrl)
            self._code = 404
            rsp = requests.get(tokenUrl, verify = False, timeout = (4, 8))
            self._code = rsp.status_code
            logging.debug('get login token: rsp.status_code - %d' %rsp.status_code)
            if rsp.status_code == 200:
                js = rsp.json()
                logging.debug(json.dumps(js, indent = 4, ensure_ascii = False))
                if 'code' in js and js['code'] == 0:
                    loginToken = js['data']['loginToken']
                    ret = True
        except:
            traceback.print_exc()
        finally:
            logging.debug('serverAPI.getLoginToken %s.' %('success' if ret else 'failed'))
            return ret, loginToken

    # 登录
    def login(self):
        logging.debug('serverAPI.login() start.')
        ret, loginToken = self.getLoginToken()
        if ret:
            try:
                ret = False
                robotId = self._cbGetRobotId()
                loginUrl = self._hostName + ':' + self._portNumber + LOGIN_URL_POSTFIX
                logging.debug('login: url - %s, loginToken - %s, robotId - %s' %(loginUrl, loginToken, robotId))
                headers = {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
                        'access_token': loginToken
                        }
                payload = { 'robotId': robotId }
                self._code = 404
                rsp = requests.post(loginUrl, headers = headers, data = payload, verify = False, timeout = (4, 8))
                self._code = rsp.status_code
                logging.debug('login: rsp.status_code - %d', rsp.status_code)
                if rsp.status_code == 200:
                    js = rsp.json()
                    logging.debug(json.dumps(js, indent = 4, ensure_ascii = False))
                    if 'code' in js and js['code'] == 0:
                        self._token = js['data']['token']
                        ret = True
            except:
                traceback.print_exc()
        logging.debug('serverAPI.login() %s' %('success' if ret else 'failed'))
        return ret, self._token

    # 获取配置信息
    def getConfig(self):
        logging.debug('serverAPI.getConfig() start.')
        ret, vsvrIp, vsvrPort = False, '', 0
        try:
            configUrl = self._hostName + ':' + self._portNumber + CONFIG_URL_POSTFIX
            logging.debug('get config: url - %s, access_token - %s' %(configUrl, self._token))
            headers = { 'access_token': self._token }
            self._code = 404
            rsp = requests.get(configUrl, headers = headers, verify = False, timeout = (4, 8))
            self._code = rsp.status_code
            logging.debug('get config: rsp.status_code - %d', rsp.status_code)
            if rsp.status_code == 200:
                js = rsp.json()
                logging.debug(json.dumps(js, indent = 4, ensure_ascii = False))
                if 'code' in js and js['code'] == 0:
                    if 'data' in js:
                        # 获取机器人编码配置
                        if 'rbtId' in js['data']:
                            self._rbtId = js['data']['rbtId']
                            logging.debug('get config: rbtId - %s' %self._rbtId)

                        # 获取视频服务器配置
                        if 'videoServer' in js['data']:
                            vsvrIp = js['data']['videoServer']['vsvrIp']
                            vsvrPort = int(js['data']['videoServer']['vsvrPort'])
                            logging.debug('get config: vsvrIp - %s, vsvrPort: %d' %(vsvrIp, vsvrPort))

                        # 获取居民配置信息
                        if 'person' in js['data']:
                            del self._personList[:]
                            for index in range(len(js['data']['person'])):
                                person = {
                                        'id':           js['data']['person'][index]['id'],
                                        'rbtId':        js['data']['person'][index]['rbtId'],
                                        'sensorId':     js['data']['person'][index]['sensorId'],
                                        'personId':     js['data']['person'][index]['personId'],
                                        'monitorInv':   js['data']['person'][index]['monitorInv'],
                                        'systolicPre':  0,
                                        'diastolicPre': 0,
                                        'heartRate':    0
                                        }
                                self._personList.append(person)
                                logging.debug('get config: person %d - id = %s, rbtId = %s, sensorId = %s, personId = %s, monitorInv = %d' %(index + 1, person['id'], person['rbtId'], person['sensorId'], person['personId'], person['monitorInv']))
                        ret = True
        except:
            traceback.print_exc()
        finally:
            logging.debug('serverAPI.getConfig() %s.' %('success' if ret else 'failed'))
            return ret, vsvrIp, vsvrPort, self._personList

    # 获取机器人固件信息
    def getVersion(self):
        logging.debug('serverAPI.getVersion() start.')
        ret, ver, url, md5 = False, None, None, None
        try:
            versionUrl = self._hostName + ':' + self._portNumber + VERSION_URL_POSTFIX
            logging.debug('get version: url - %s, token - %s' %(versionUrl, self._token))
            headers = { 'access_token': self._token }
            self._code = 404
            rsp = requests.get(versionUrl, headers = headers, verify = False, timeout = (4, 8))
            self._code = rsp.status_code
            logging.debug('get version: rsp.status_code - %d', rsp.status_code)
            if rsp.status_code == 200:
                js = rsp.json()
                logging.debug(json.dumps(js, indent = 4, ensure_ascii = False))
                if 'code' in js and js['code'] == 0:
                    if js['data']['version']:
                        ver = str(js['data']['version'])
                    if js['data']['url']:
                        url = js['data']['url']
                    if js['data']['md5Num']:
                        md5 = js['data']['md5Num']
                    ret = True
                    logging.debug('serverAPI.getVersion(): version - %s, url - %s, md5 - %s' %(ver, url, md5))
        except:
            traceback.print_exc()
        finally:
            logging.debug('serverAPI.getVersion() %s.' %('success' if ret else 'failed'))
            return ret, ver, url, md5

    # 计算文件的 md5
    def md5Sum(self, fileName):
        logging.debug('serverAPI.md5Sum(%s).' %fileName)
        fd = open(fileName, 'r')
        content = fd.read()
        fd.close()
        md5 = hashlib.md5(content).hexdigest()
        logging.debug('serverAPI.md5Sum(%s) - %s' %(fileName, md5))
        return md5

    # 备份配置文件
    def backupConfig(self):
        logging.debug('serverAPI.backupConfig().')
        baseDir = os.path.dirname(os.path.abspath(__file__))
        fileName = 'data.ini'
        filePath = os.path.join(baseDir, '../data_access', fileName)
        logging.debug('serverAPI.backupConfig(). filePath - %s' %filePath)
        if os.path.isfile(filePath):
            logging.debug('sudo cp %s /home/pi/%s' %(filePath, fileName))
            os.system('sudo cp %s /home/pi/%s' %(filePath, fileName))

    # 运行软件
    def runSoftware(self, dirName):
        logging.debug('serverAPI.runSoftware().')

        ret = False
        filePath = '%s/backend/server.py' %dirName
        if not os.path.isfile(filePath):
            filePath = '%s/backend/server.pyc' %dirName
        if os.path.isfile(filePath):
            logging.debug('python %s &' %filePath)
            os.system('python %s &' %filePath)
            ret = True
        time.sleep(1)
        return ret

    # 更新软件
    def updateSoftware(self):
        logging.debug('serverAPI.updateSoftware().')
        ret = False
        filePath = None
        newDName = None
        try:
            curVer = version.getVersion()
            logging.debug('current version: %s' %curVer)
            ret, newVer, fileUrl, md5 = self.getVersion()
            if ret:
                logging.debug('latest version: %s' %newVer)

            if ret and float(newVer) > float(curVer):
                logging.debug('download latest software...')

                self.backupConfig()     # 备份数据文件
                curFName = 'robot-v' + str(curVer)
                newFName = 'robot-v' + str(newVer) + '.tar'

                # 下载新的软件
                filePath = os.path.join(self._updateDir, newFName)
                logging.debug('software path: %s' %filePath)
                if os.path.isfile(filePath):
                    logging.debug('%s' %filePath)
                    os.remove(filePath)

                logging.debug('download software: version - %s, url - %s' %(newVer, fileUrl))
                headers = { 'access_token': self._token }
                self._code = 404
                rsp = requests.get(fileUrl, headers = headers, stream = True, verify = False, timeout = (4, 8))
                self._code = rsp.status_code
                logging.debug('download software: rsp.status_code - %d', rsp.status_code)
                if rsp.status_code == 200:
                    fileSize = int(rsp.headers['Content-Length'])
                    logging.debug('download software: file size - %d bytes', fileSize)
                    chunks = 0
                    with open(filePath, 'wb') as verFile:
                        for chunk in rsp.iter_content(chunk_size = 16 * 1024):
                            if chunk:
                                chunks += 1
                                verFile.write(chunk)
                                logging.debug('download software: size - %sk bytes' %(format(chunks * 16, ',')))

                    if os.path.isfile(filePath):
                        size = os.path.getsize(filePath)
                        md5Sum = self.md5Sum(filePath)
                        if size != fileSize:
                            logging.debug('download software failed: url - %s, file size - %d, download size - %d' %(fileUrl, fileSize, size))
                            logging.debug('remove file: %s' %filePath)
                            if os.remove(filePath) != 0:
                                raise 'abort'
                        elif md5 != md5Sum:
                            logging.debug('download software failed: url - %s, md5 not match - %s vs %s' %(fileUrl, md5, md5Sum))
                            logging.debug('remove file: %s' %filePath)
                            if os.remove(filePath) != 0:
                                raise 'abort'
                        else:
                            logging.debug('download software done: url - %s' %fileUrl)
                            newDName = '/home/pi/robot-v' + str(newVer)
                            if os.path.isdir(newDName):
                                logging.debug('sudo rm -rf %s' %newDName)
                                if os.system('sudo rm -rf %s' %newDName) != 0:
                                    raise 'abort'
                            logging.debug('sudo mkdir %s' %newDName)
                            if os.system('sudo mkdir %s' %newDName) != 0:
                                raise 'abort'
                            logging.debug('sudo tar -xvf %s -C %s' %(filePath, newDName))
                            if os.system('sudo tar -xvf %s -C %s' %(filePath, newDName)) != 0:
                                raise 'abort'
                            os.remove(filePath)
                            filePath = None

                            # 恢复配置文件
                            srcConfig = '/home/pi/data.ini'
                            dstConfig = newDName + '/backend/data_access/data.ini'
                            if os.path.isfile(srcConfig):
                                logging.debug('sudo cp %s %s' %(srcConfig, dstConfig))
                                if os.system('sudo cp %s %s' %(srcConfig, dstConfig)) != 0:
                                    raise 'abort'

                            ret = True
                            # 启动新的程序
                            p = Process(target = self.runSoftware, args = (newDName, ))
                            p.start()
                            time.sleep(1)
                            logging.debug('sudo kill %d' %os.getpid())
                            os.system('sudo kill %d' %os.getpid())
        except:
            traceback.print_exc()
            if newDName:
                if os.path.isdir(newDName):
                    os.system('sudo rm -rf %s' %newDName)
        finally:
            if filePath:
                if os.path.isfile(filePath):
                    os.remove(filePath)

            logging.debug('serverAPI.updateSoftware() %s.' %('success' if ret else 'fail'))
            return ret

    # 获取音频列表
    def getMp3List(self):
        logging.debug('serverAPI.getMp3List() start.')
        ret, mp3List = False, []
        try:
            mp3ListUrl = self._hostName + ':' + self._portNumber + MP3_LIST_URL_POSTFIX
            logging.debug('get mp3 list: url - %s, token - %s' %(mp3ListUrl, self._token))
            headers = { 'access_token': self._token }
            self._code = 404
            rsp = requests.get(mp3ListUrl, headers = headers, verify = False, timeout = (4, 8))
            self._code = rsp.status_code
            logging.debug('get mp3 list: rsp.status_code - %d', rsp.status_code)
            if rsp.status_code == 200:
                js = rsp.json()
                logging.debug(json.dumps(js, indent = 4, ensure_ascii = False))
                if 'code' in js and js['code'] == 0:
                    for index in range(len(js['data'])):
                        if js['data'][index]['fileId']:
                            mp3 = {
                                    'fileId':   js['data'][index]['fileId'],
                                    'fileName': js['data'][index]['fileName'],
                                    'pri':      js['data'][index]['pri']
                                    }
                            mp3List.append(mp3)
                            logging.debug('get mp3 list: mp3 %d - fileId = %s, fileName = %s, pri = %s' %(index + 1, mp3['fileId'], mp3['fileName'], mp3['pri']))
                    ret = True
        except:
            traceback.print_exc()
        finally:
            logging.debug('serverAPI.getMp3List() %s.' %('success' if ret else 'failed'))
            return ret, mp3List

    # 获取居民列表
    def getPersonList(self):
        return self._personList

    # 获取在线医生列表
    def getDoctorList(self, personId):
        logging.debug('serverAPI.getDoctorList() start.')
        ret, doctorList = False, []
        try:
            doctorListUrl = self._hostName + ':' + self._portNumber + DOCTOR_LIST_URL_POSTFIX
            logging.debug('get doctor list: url - %s, token - %s, personId - %s' %(doctorListUrl, self._token, personId))
            headers = {
                    'Content-Type': 'application/json;charset=UTF-8',
                    'access_token': self._token
                    }
            payload = { 'personId': personId }
            self._code = 404
            rsp = requests.post(doctorListUrl, headers = headers, data = json.dumps(payload), verify = False, timeout = (4, 8))
            self._code = rsp.status_code
            logging.debug('get doctor list: rsp.status_code - %d', rsp.status_code)
            if rsp.status_code == 200:
                js = rsp.json()
                logging.debug(json.dumps(js, indent = 4, ensure_ascii = False))
                if 'code' in js and js['code'] == 0:
                    for index in range(len(js['data'])):
                        doctor = {
                                'id':   js['data'][index]['id'],
                                'name': js['data'][index]['name'],
                                'role': js['data'][index]['role'],
                                }
                        doctorList.append(doctor)
                    ret = True
        except:
            traceback.print_exc()
        finally:
            logging.debug('serverAPI.getDoctorList() %s.' %('success' if ret else 'failed'))
            return ret, doctorList

    # 保存呼叫记录
    def saveCallLog(self, doctorId, callSts, tmStr, tmEnd = None):
        ret = False
        logging.debug('serverAPI.saveCallLog() start ...')
        try:
            callLogUrl = self._hostName + ':' + self._portNumber + CALL_LOG_URL_POSTFIX
            logging.debug('saveCallLog: url - %s, token - %s' %(callLogUrl, self._token))
            headers = {
                    'Content-Type': 'application/json;charset=UTF-8',
                    'access_token': self._token
                    }
            payload = { 'callType': 0,
                        'personId': self._rbtId,
                        'doctorId': doctorId,
                        'callSts':  1 if callSts else 0,
                        'tmStr':    time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(tmStr)) }
            if tmEnd:
                payload['tmEnd'] = time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(tmEnd))
            logging.debug(json.dumps(payload, indent = 4, ensure_ascii = False))

            self._code = 404
            rsp = requests.post(callLogUrl, headers = headers, data = json.dumps(payload), verify = False, timeout = (4, 8))
            self._code = rsp.status_code
            logging.debug('saveCallLog: rsp.status_code - %d', rsp.status_code)
            if rsp.status_code == 200:
                js = rsp.json()
                logging.debug(json.dumps(js, indent = 4, ensure_ascii = False))
                if 'code' in js and js['code'] == 0:
                    ret = True
        except:
            traceback.print_exc()
        finally:
            logging.debug('serverAPI.saveCallLog() %s.' %('success' if ret else 'failed'))
            return ret

    # 心跳同步
    def heatbeat(self, hvx = None, playVer = None, confVer = None):
        ret, playUpdate, confUpdate, softUpdate = False, None, None, None
        logging.debug('serverAPI.heartbeat() start ...')
        try:
            heatbeatUrl = self._hostName + ':' + self._portNumber + HEATBEAT_URL_POSTFIX
            logging.debug('heartbeat: url - %s, token - %s' %(heatbeatUrl, self._token))
            headers = {
                    'Content-Type': 'application/json;charset=UTF-8',
                    'access_token': self._token
                    }
            payload = { 'rbtId': self._rbtId,
                        'data': [] }
            for person in self._personList:
                data = {}
                data['personId'] = person['personId']
                data['systolicPre'] = 0
                data['diastolicPre'] = 0
                data['heartRate'] = 0
                data['gps'] = self._gps
                if hvx:
                    if 'diastolicPre' in hvx:
                        data['systolicPre'] = hvx['systolicPre']
                    if 'diastolicPre' in hvx:
                        data['diastolicPre'] = hvx['diastolicPre']
                    if 'heartRate' in hvx:
                        data['heartRate'] = hvx['heartRate']
                payload['data'].append(data)
            if playVer:
                payload['playVer'] = playVer
            if confVer:
                payload['confVer'] = confVer

            logging.debug('headers: - %s' %(json.dumps(headers, indent = 4, ensure_ascii = False)))
            logging.debug('message: - %s' %(json.dumps(payload, indent = 4, ensure_ascii = False)))

            self._code = 404
            rsp = requests.post(heatbeatUrl, headers = headers, data = json.dumps(payload), verify = False, timeout = (4, 8))
            self._code = rsp.status_code
            logging.debug('heatbeat: rsp.status_code - %d', rsp.status_code)
            if rsp.status_code == 200:
                js = rsp.json()
                logging.debug(json.dumps(js, indent = 4, ensure_ascii = False))
                if 'code' in js and js['code'] == 0:
                    # 检查配置信息是否有更新
                    if 'confVer' in js['data']:
                        logging.debug('heatbeat: conf version updated - %d.', js['data']['confVer'])
                        confUpdate = js['data']['confVer']

                    # 检查音频列表是否有更新
                    if 'playVer' in js['data']:
                        logging.debug('heatbeat: play version updated - %d.', js['data']['playVer'])
                        playUpdate = js['data']['playVer']

                    # 检查机器人固件版本号
                    if 'ver' in js['data']:
                        logging.debug('heatbeat: soft version updated - %d.', js['data']['ver'])
                        softUpdate = js['data']['ver']

                    ret = True
        except:
            traceback.print_exc()
        finally:
            logging.debug('serverAPI.heartbeat() %s.' %('success' if ret else 'failed'))
            return ret, playUpdate, confUpdate, softUpdate


################################################################################
# 测试程序
if __name__ == '__main__':
    api = serverAPI(hostName = 'http://111.12.194.250', portNumber = '80')
    # 测试登录
    ret, _ = api.login()
    if ret:
        # 测试获取配置
        ret, vsvrIp, vsvrPort, personList = api.getConfig()
        if ret:
            print(vsvrIp, vsvrPort, personList)
            # 测试获取医生列表
            ret, doctorList = api.getDoctorList(personList[0]['personId'])
            if ret:
                print(doctorList)

        # 测试获取音频列表
        ret, mp3List = api.getMp3List()
        if ret:
            print(mp3List)

        # 测试心跳同步
        ret, playUpdate, confUpdate, softUpdate = api.heatbeat()
        if ret:
            print(playUpdate, confUpdate, softUpdate)

        # 测试保存呼叫记录
        tmStr = time.time()
        time.sleep(5)
        tmEnd = time.time()
        api.saveCallLog(doctorId = 'jove', callSts = True, tmStr = tmStr, tmEnd = tmEnd)

        # 测试下载软件
        # api.getVersion()
        api.updateSoftware()

