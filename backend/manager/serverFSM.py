#!/usr/bin/python
# -*- coding: utf8 -*-

import os
import time
import Queue
import logging
import platform
import threading
from transitions import Machine, State

if __name__ == '__main__':
    import sys
    sys.path.append('..')

from utility import setLogging
from utility import audioRecord
from utility import robotId
from utility import version, ramfs

from manager.mp3FSM import mp3FSM
from manager.lcdFSM import lcdFSM
from manager.serverAPI import serverAPI
from manager.buttonAPI import buttonAPI
if platform.system().lower() == 'linux':
    from manager.imxFSM import imxFSM
    from manager.bandFSM import bandFSM
    from data_access import bracelet

__all__ = [
        'serverFSM',
        ]

base_dir = os.path.dirname(os.path.abspath(__file__))
if platform.system().lower() == 'windows':
    HEARTBEAT_INV = 1 * 60
    CALL_SOUND_FILEPATH = os.path.join(base_dir, '..\static\mp3\\dudu.mp3')
elif platform.system().lower() == 'linux':
    HEARTBEAT_INV = 1 * 60
    CALL_SOUND_FILEPATH = os.path.join(base_dir, '../static/mp3/dudu.mp3')
else:
    raise NotImplementedError

# 系统服务器状态机类定义
class serverFSM(object):
    # 初始化
    def __init__(self, hostName, portNumber, cbGetRobotId = robotId.robotIdSim, heartbeatInv = HEARTBEAT_INV):
        self._hostName = hostName
        self._portNumber = portNumber
        self._heartbeatInv = heartbeatInv
        self._serverAPI = serverAPI(hostName = hostName, portNumber = portNumber, cbGetRobotId = cbGetRobotId)

        self._silence = False
        self._confVer = None
        self._playVer = None
        self._softVer = 1.01
        self._confUpdated = False
        self._playUpdated = False

        self._loginThread = None
        self._loginFiniEvent = threading.Event()
        self._loginDoneEvent = threading.Event()

        self._configThread = None
        self._configFiniEvent = threading.Event()
        self._configDoneEvent = threading.Event()

        self._updateThread = None
        self._updateFiniEvent = threading.Event()
        self._updateDoneEvent = threading.Event()
        self._updateDir = ramfs.ramfsInit()

        self._heartbeatThread = None
        self._heartbeatFiniEvent = threading.Event()
        self._heartbeatDoneEvent = threading.Event()

        self._vsvrIp = None
        self._vsvrPort = None
        self._personId = None

        self._mp3FSM = None
        self._imxFSM = None
        self._bandFSM = None
        self._lcdFSM = lcdFSM()
        self._buttonAPI = buttonAPI()
        self._buttonAPI.setPowerCallback(self._lcdFSM._lcdAPI.backlit_switch)
        self._buttonAPI.setIncVolumeCallback(audioRecord.incVolume)
        self._buttonAPI.setDecVolumeCallback(audioRecord.decVolume)

        self._states = [
            State(name = 'stateLogin',      on_enter = 'actLogin',      ignore_invalid_triggers = True),
            State(name = 'stateLogined',    on_enter = 'actLogined',    ignore_invalid_triggers = True),
        ]
        self._transitions = [
            # 初始状态 ------->
            {
                'trigger':  'evtLoginOk',
                'source':   'stateLogin',
                'dest':     'stateLogined'
            },
            # 登录状态 ------->
            {
                'trigger':  'evtOffline',
                'source':   'stateLogined',
                'dest':     'stateLogin',
                'before':   'actOffline'
            },
        ]

        # 初始化麦克风灵敏度和扬声器音量
        audioRecord.captureInit()
        audioRecord.volumeInit()

        # 播放启动音效
        audioRecord.soundStartup(wait = True)

        # 启动状态机线程
        self._eventList = []
        self._eventQueue = Queue.Queue(5)
        self._machine = Machine(self, states = self._states, transitions = self._transitions, ignore_invalid_triggers = True)
        self._fsmDoneEvent = threading.Event()
        self._fsmThread = threading.Thread(target = self.fsmThread)
        self._fsmThread.start()

    # 向系统服务器状态机事件队列中放事件
    def putEvent(self, desc, event):
        if self._eventQueue:
            v = [desc, event]
            if v not in self._eventList and not self._eventQueue.full():
                logging.debug('serverFSM.putEvent(%s).' %desc)
                self._eventList.append(v)
                self._eventQueue.put(v)
                return True
        return False

    # 从系统服务器状态机事件队列中取事件
    def getEvent(self):
        if self._eventQueue:
            v = self._eventQueue.get(block = True)
            self._eventQueue.task_done()
            logging.debug('serverFSM.getEvent(%s).' %v[0])
            if v in self._eventList:
                self._eventList.remove(v)
            return True, v[0], v[1]
        return False, None, None

    # 登录服务器
    #   启动后台线程登录服务器
    def actLogin(self):
        logging.debug('serverFSM.actLogin().')
        self.loginInit()

    # 启动登录
    def loginInit(self):
        logging.debug('serverFSM.loginInit().')
        if not self._loginThread:
            self._loginThread = threading.Thread(target = self.loginThread)
            self._loginThread.start()

    # 终止登录
    def loginFini(self):
        logging.debug('serverFSM.loginFini().')
        if self._loginThread:
            self._loginFiniEvent.set()
            self._loginDoneEvent.wait()

    # 后台登录线程
    #   如果登录失败，每隔 30s 重新登录
    def loginThread(self):
        logging.debug('serverFSM.loginThread().')
        try:
            self._loginDoneEvent.clear()
            self._loginFiniEvent.clear()
            while True:
                ret, self._token = self._serverAPI.login()
                if ret:
                    raise Exception('login')
                logging.debug('login failed')
                logging.debug('retry to login in 30s.')
                self._loginFiniEvent.wait(30)
                if self._loginFiniEvent.isSet():
                    raise Exception('fini')
        except Exception, e:
            if e.message == 'login':
                # 登录成功
                self.updateInit()           # 启动检查软件更新线程
                self._lcdFSM.lcdIdle()
                if not self._silence:
                    audioRecord.soundConnected(wait = True)
                self._silence = True
                self.putEvent('evtLoginOk', self.evtLoginOk)
        finally:
            logging.debug('serverFSM.loginThread() fini.')
            self._loginThread = None
            self._loginDoneEvent.set()

    # 登录服务器后处理
    def actLogined(self):
        logging.debug('serverFSM.actLogined().')
        self.configInit()           # 获取配置文件

    # 开始配置
    def configInit(self):
        logging.debug('serverFSM.configInit().')
        if not self._configThread:
            self._configThread = threading.Thread(target = self.configThread)
            self._configThread.start()

    # 终止配置
    def configFini(self):
        logging.debug('serverFSM.configFini().')
        if self._configThread:
            self._configFiniEvent.set()
            self._configDoneEvent.wait()

    # 后台获取配置
    #   尝试 6 次失败后，掉线处理
    def configThread(self):
        logging.debug('serverFSM.configThread().')
        try:
            self._configDoneEvent.clear()
            self._configFiniEvent.clear()
            for retry in range(0, 6):
                ret, vsvrIp, vsvrPort, personList = self._serverAPI.getConfig()
                if ret:
                    raise Exception('config')
                logging.debug('get config failed.')
                logging.debug('retry to get config in 1s.')
                self._configFiniEvent.wait(1)
                if self._configFiniEvent.isSet():
                    raise Exception('fini')
            raise Exception('abort')
        except Exception, e:
            if e.message == 'config':
                # 创建音频状态机
                if not self._mp3FSM:
                    self._mp3FSM = mp3FSM(hostName = self._hostName,
                                          portNumber = self._portNumber,
                                          token = self._token,
                                          getMp3List = self.getMp3List,
                                          lcdPlay = self._lcdFSM.lcdPlay,
                                          lcdIdle = self._lcdFSM.lcdIdle)
                    self._buttonAPI.setPlayCallback(self._mp3FSM.cbButtonPlay)
                    if platform.system().lower() == 'windows':
                        self._buttonAPI.setRadioCallback(self._mp3FSM.cbButtonRadio)
                        self._buttonAPI.setImxCallback(self._mp3FSM.cbButtonImx)
                else:
                    self._mp3FSM.reconfig(hostName = self._hostName,
                                          portNumber = self._portNumber,
                                          token = self._token)

                if platform.system().lower() == 'linux':
                    # 配置视频服务器
                    personId = personList[0]['personId']
                    if not self._imxFSM:
                        self._vsvrIp = vsvrIp
                        self._vsvrPort = vsvrPort
                        self._personId = personId
                        self._imxFSM = imxFSM(server = vsvrIp,
                                              port = vsvrPort,
                                              personId = personId,
                                              getDoctorList = self.getDoctorList,
                                              saveCallLog = self._serverAPI.saveCallLog,
                                              lcdImx = self._lcdFSM.lcdImx,
                                              lcdIdle = self._lcdFSM.lcdIdle)
                        self._imxFSM.setExitIdleCallback(self.cbEntryImxMode)
                        self._imxFSM.setEntryIdleCallback(self.cbExitImxMode)
                        self._buttonAPI.setCallCallback(self._imxFSM.cbButtonCall)
                        self._buttonAPI.setMuteCallback(self._imxFSM.cbButtonMute)
                    elif self._vsvrIp != vsvrIp or self._vsvrPort != vsvrPort or self._personId != personId:
                        # 视频服务器发生变化，重新启动
                        logging.debug('serverFSM.configThread() vsvr changed, reboot...')
                        os.system('sudo reboot')

                    # 配置蓝牙手环
                    if not self._bandFSM:
                        _, mac = bracelet.get_bracelet_mac(1)
                        self._bandFSM = bandFSM(mac = mac)

                    # 启动心跳同步线程
                    self.heatbeatInit()
                self._confUpdated = True     # 配置更新成功
            elif e.message == 'abort':
                logging.debug('serverFSM.configThread() failed...')
                self._silence = True if self._serverAPI._code == 200 else False
                self.offline()
        finally:
            logging.debug('serverFSM.configThread() fini.')
            self._configThread = None
            self._configDoneEvent.set()

    # 网络掉线处理
    def offline(self):
        logging.debug('serverFSM.offline().')
        self._lcdFSM.lcdOffline()
        if not self._silence:
            audioRecord.soundOffline(wait = True)
        self.putEvent('evtOffline', self.evtOffline)

    # 掉线处理
    def actOffline(self):
        logging.debug('serverFSM.actOffline().')
        try:
            if self._imxFSM:
                self._imxFSM.offline()
            if self._mp3FSM:
                self._mp3FSM.offline()
        except:
            pass
        finally:
            pass

    # 获取医生列表
    def getDoctorList(self, personId):
        try:
            ret, doctorList = False, None
            logging.debug('serverFSM.getDoctorList().')
            if self.state == 'stateLogined':
                for retry in range(0, 6):
                    ret, doctorList = self._serverAPI.getDoctorList(personId)
                    if ret:
                        break
        except:
            pass
        finally:
            if not ret:
                self._silence = True if self._serverAPI._code == 200 else False
                self.offline()
            return ret, doctorList

    # 获取音频列表
    def getMp3List(self):
        try:
            ret, mp3List = False, None
            logging.debug('serverFSM.getMp3List().')
            if self.state == 'stateLogined':
                for retry in range(0, 6):
                    ret, mp3List = self._serverAPI.getMp3List()
                    if ret:
                        break
        except:
            pass
        finally:
            if not ret:
                self._silence = True if self._serverAPI._code == 200 else False
                self.offline()
            return ret, mp3List

    # 设置音频更新完成
    def playUpdated(self):
        logging.debug('serverFSM.playUpdated().')
        self._playUpdated = True

    # 设置配置更新完成
    def confUpdated(self):
        logging.debug('serverFSM.confUpdated().')
        self._confUpdated = True

    # 进入视频通话模式回调函数
    def cbEntryImxMode(self):
        try:
            logging.debug('serverFSM.cbEntryImxMode().')
            if self._mp3FSM:
                self._mp3FSM.actImxOn()
                self._mp3FSM.putEvent('evtImxOn', self._mp3FSM.evtImxOn)
        except:
            pass
        finally:
            pass

    # 退出视频通话模式回调函数
    def cbExitImxMode(self):
        try:
            logging.debug('serverFSM.cbExitImxMode().')
            if self._mp3FSM:
                self._mp3FSM.actImxOff()
                self._mp3FSM.putEvent('evtImxOff', self._mp3FSM.evtImxOff)
        except:
            pass
        finally:
            pass

    # 获取版本信息
    def getVersion(self):
        logging.debug('serverFSM.getVersion().')
        ret, newVer, _, _ = self._serverAPI.getVersion()
        if ret:
            return ret, newVer
        return False, None

    # 软件更新检测线程
    #   间隔 10 分钟检测一次
    def updateThread(self):
        logging.debug('serverFSM.updateThread().')
        loop = True
        while loop:
            try:
                self._updateFiniEvent.clear()
                self._updateDoneEvent.clear()
                self._serverAPI.updateSoftware()
                self._updateFiniEvent.wait(10 * 60)
                if self._updateFiniEvent.isSet():
                    raise 'fini'
            except Exception, e:
                if e.message == 'fini':
                    logging.debug('serverFSM.updateThread() fini.')
                    self._updateThread = None
                    self._updateDoneEvent.set()
                    loop = False
            finally:
                pass

    # 开始软件更新检测
    def updateInit(self):
        logging.debug('serverFSM.updateInit().')
        if not self._updateThread:
            version.runVersion()
            self._updateThread = threading.Thread(target = self.updateThread)
            self._updateThread.start()

    # 终止软件更新检测
    def updateFini(self):
        logging.debug('serverFSM.updateFini().')
        if self._updateThread:
            self._updateFiniEvent.set()
            self._updateDoneEvent.wait()

    # 后台心跳同步处理
    #   尝试 6 次后失败后，重新热启动
    def heartbeatThread(self):
        logging.debug('serverFSM.heartbeatThread().')
        try:
            while True:
                # 等待同步时间间隔
                self._heartbeatDoneEvent.clear()
                self._heartbeatFiniEvent.clear()

                ret = False
                for retry in range(0, 6):
                    hvx = self._bandFSM.getHvx()
                    ret, playVer, confVer, softVer = self._serverAPI.heatbeat(hvx = hvx,
                                                                              playVer = self._playVer if self._playUpdated else None,
                                                                              confVer = self._confVer if self._confUpdated else None)
                    if ret:
                        if playVer and playVer != self._playVer:
                            self._playVer = playVer
                            self._playUpdated = False
                            if self._mp3FSM:
                                self._mp3FSM.updateInit(self.playUpdated)   # 更新音频列表
                        if confVer and confVer != self._confVer:
                            self._confVer = confVer
                            self._confUpdated = False
                            self.configInit()                           # 重新更新配置
                        break
                    self._heartbeatFiniEvent.wait(1)
                    if self._heartbeatFiniEvent.isSet():
                        raise Exception('fini')
                if ret:
                    self._heartbeatFiniEvent.wait(self._heartbeatInv)
                    if self._heartbeatFiniEvent.isSet():
                        raise Exception('fini')
                else:
                    logging.debug('heartbeat failed.')
                    self._silence = True if self._serverAPI._code == 200 else False
                    self.offline()
                    break
        except Exception as e:
            pass
        finally:
            logging.debug('serverFSM.heartbeatThread() fini.')
            self._heartbeatThread = None
            self._heartbeatDoneEvent.set()

    # 启动心跳同步
    def heatbeatInit(self):
        logging.debug('serverFSM.heatbeatInit().')
        if not self._heartbeatThread:
            self._heartbeatThread = threading.Thread(target = self.heartbeatThread)
            self._heartbeatThread.start()

    # 终止心跳同步
    def heatbeatFini(self):
        logging.debug('serverFSM.heatbeatFini().')
        if self._heartbeatThread:
            self._heartbeatFiniEvent.set()
            self._heartbeatDoneEvent.wait()

    # 系统服务器状态机后台线程
    def fsmThread(self):
        logging.debug('serverFSM.fsmThread().')
        try:
            self._fsmDoneEvent.clear()
            self.to_stateLogin()    # 切换到登录状态
            while True:
                ret, desc, event = self.getEvent()
                if ret:
                    if desc == 'fini':
                        raise Exception('fini')
                    else:
                        event()
                        logging.debug('serverFSM: state - %s', self.state)
        finally:
            self.loginFini()
            self.configFini()
            self.heatbeatFini()
            self.updateFini()

            self._eventQueue.queue.clear()
            self._eventQueue = None
            del self._eventList[:]
            logging.debug('serverFSM.fsmThread() fini.')
            self._fsmThread = None
            self._fsmDoneEvent.set()

    # 终止系统服务器状态机
    def fini(self):
        logging.debug('serverFSM.fini().')
        if self._mp3FSM:
            self._mp3FSM.fini()
        if platform.system().lower() == 'linux':
            if self._imxFSM:
                self._imxFSM.fini()
            if self._bandFSM:
                self._bandFSM.fini()
            if self._lcdFSM:
                self._lcdFSM.fini()
        if self._fsmThread:
            self.putEvent('fini', None)
            self._fsmDoneEvent.wait()


###############################################################################
# 测试程序
if __name__ == '__main__':
    try:
        fsm = serverFSM(hostName = 'http://111.12.194.250', portNumber = '80')
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        fsm.fini()
        sys.exit(0)
