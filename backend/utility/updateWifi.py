#!/usr/bin/python
# -*- coding: utf8 -*-

import time
import os, platform
import logging
import traceback

if __name__ == '__main__':
    import sys
    sys.path.append('..')

from utility import setLogging

__all__ = [
        'updateWifi'
        ]

WIFI_FILENAME = 'wpa_supplicant.conf'
if platform.system().lower() == 'linux':
    WIFI_SAVEFILE = '/etc/wpa_supplicant/wpa_supplicant.conf'

# 更新无线网络
def updateWifi(ssid = 'raisecom', psk = 'wjw@201910', priority = 1):
    logging.debug('updateWifi(): ssid - %s, psk - %s, priority - %s' %(ssid, psk, priority))
    try:
        # 删除之前的无线配置文件
        baseDir = os.path.dirname(os.path.abspath(__file__))
        filePath = os.path.join(baseDir, WIFI_FILENAME)
        logging.debug('updateWifi(): filePath - %s' %filePath)
        if os.path.isfile(filePath):
            os.remove(filePath)

        # 创建新的无线配置文件
        with open(filePath, 'w') as wifiFile:
            contents = [
                'ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev\n',
                'update_config=1\n',
                'country=CN\n',
                '\n',
                'network={\n',
                '    ssid=\"robot-cfg\"\n',
                '    psk=\"onlyforrobot\"\n',
                '    priority=5\n',
                '}\n'
                '\n',
                'network={\n',
                '    ssid=\"%s\"\n' %str(ssid),
                '    psk=\"%s\"\n' %str(psk),
                '    priority=%s\n' %str(priority),
                '}'
            ]
            wifiFile.writelines(contents)
        if platform.system().lower() == 'linux':
            os.system('sudo cp %s %s' %(filePath, WIFI_SAVEFILE))
            time.sleep(1)
            if os.path.isfile(filePath):
                os.remove(filePath)
    except:
        traceback.print_exc()
    finally:
        # TODO
        pass


################################################################################
# 测试程序
if __name__ == '__main__':
    updateWifi('raisecom', 'wjw@201910', '1')
