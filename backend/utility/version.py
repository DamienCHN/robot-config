#!/usr/bin/python
# -*- coding: utf8 -*-

import os
import time
import logging
import traceback

logging.basicConfig(level = logging.DEBUG,
                    format = ' %(asctime)s - %(filename)s[line:%(lineno)d] - %(thread)d - %(levelname)s - %(message)s')

VERSION = '1.02'

VERSION_PRE = 'robot-v'
LINK = 'robot-ln'
ROOT_DIR = '/home/pi'

# 获取版本信息
def getVersion():
    logging.debug('version.getVersion().')
    return VERSION

# 运行程序版本
def runVersion():
    logging.debug('version.runVersion().')

    # 检查软链接地址
    fd = os.popen('ls %s -al | grep -w %s' %(ROOT_DIR, LINK))
    content = fd.read()
    fd.close()

    logging.debug('%s' %content)

    updated = False
    linkPath = ROOT_DIR + '/' + LINK
    currPath = ROOT_DIR + '/' + VERSION_PRE + VERSION
    logging.debug('currPath: %s' %currPath)
    if os.path.isdir(currPath):
        try:
            if LINK not in content:
                # 软链接不存在，创建新的软链接指向当前程序
                logging.debug('ln -s %s %s' %(currPath, linkPath))
                os.system('ln -s %s %s' %(currPath, linkPath))

                # 复制 rc.local 到 /etc/rc.local
                logging.debug('sudo cp %s/rc.local /etc/rc.local' %currPath)
                os.system('sudo cp %s/rc.local /etc/rc.local' %currPath)

                updated = True
            elif LINK in content and currPath not in content:
                # 有旧的软链接，先行删除，再行创建
                logging.debug('rm -rf %s' %linkPath)
                os.system('rm -rf %s' %linkPath)

                logging.debug('ln -s %s %s' %(currPath, linkPath))
                os.system('ln -s %s %s' %(currPath, linkPath))

                # 复制 rc.local 到 /etc/rc.local
                logging.debug('sudo cp %s/rc.local /etc/rc.local' %currPath)
                os.system('sudo cp %s/rc.local /etc/rc.local' %currPath)

                updated = True
        except:
            traceback.print_exc()
        finally:
            pass

        # 删除其它版本的程序
        dirs = os.listdir('%s' %ROOT_DIR)
        for i in range(0, len(dirs)):
            path = os.path.join(ROOT_DIR, dirs[i])
            logging.debug('%s' %path)

            # 删除 imx-core 日志文件
            if os.path.isfile(path) and 'imx-core' in path:
                try:
                    logging.debug('rm -rf %s' %path)
                    os.system('rm -rf %s' %path)
                except:
                    traceback.print_exc()
                finally:
                    pass

            # 删除其它版本程序
            if os.path.isdir(path) and VERSION_PRE in path and currPath not in path:
                try:
                    logging.debug('rm -rf %s' %path)
                    os.system('rm -rf %s' %path)
                except:
                    traceback.print_exc()
                finally:
                    pass

        if updated:
            # 重新启动系统
            logging.debug('update software success, reboot it...')
            logging.debug('sudo reboot')
            time.sleep(1)
            os.system('sudo reboot')


################################################################################
# 测试程序
if __name__ == '__main__':
    runVersion()
