#!/usr/bin/python
# -*- coding: utf8 -*-

import uuid


__all__ = ["robotId"]

# 计算机器人的 id
#
#   算法说明：
#       获取主板的 MAC 地址
def robotId():
    """
    get the local machine's mac address
    """
    uuid._node = None
    node = uuid.getnode()
    return uuid.UUID(int = node).hex[-12:].lower()

def robotIdSim():
    return '001d43201897'

# 执行代码
if __name__ == '__main__':
    print(robotId())
