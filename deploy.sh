#!/bin/sh

srcDir=$(pwd)

cd backend/utility
version=$(python -c 'import version; print(version.getVersion())')

# 如果目标目录已经存在，删除后重新创建目录
dstDir=/home/pi/robot-v$version
if [ -d $dstDir ]; then
    rm -rf $dstDir
fi
mkdir $dstDir

cp $srcDir/rc.local $dstDir/rc.local

cp -r $srcDir/backend $dstDir/backend
cp -r $srcDir/frontend $dstDir/frontend

find $dstDir -name '*.pyc' | xargs rm -f
find $dstDir -name '*.py' | xargs python -m py_compile
find $dstDir -name '*.py' | xargs rm -f

cd $dstDir
if [ -f $dstDir.tar ]; then
    rm -rf $dstDir.tar
fi
tar -cvf $dstDir.tar .
rm -rf $dstDir

echo 'Done.'
